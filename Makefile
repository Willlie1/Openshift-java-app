deploy:
	./mvnw clean package fabric8:resource fabric8:build fabric8:run

test:
	./mvnw clean test

build:
	./mvnw clean package fabric8:build

clean:
	./mvnw clean