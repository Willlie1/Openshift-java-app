/**
 * Default Jenkins Pipeline implementation.
 * Load this script inside your Jenkinsfile and run the {@literal execute ( )} method:
 *
 * <pre>{@code
 * node{
 *     checkout scm
 *
 *     def defaultPipeline = load 'DefaultPipeline.groovy'
 *     defaultPipeline.execute()
 *}*}</pre>
 *
 * @author Roy Willemse
 */

/**
 * Method to detect the stage based on the name of the branch.
 * @return enum indicating stage
 */
String detectBranchType(String branchName) {
    final def DEVELOP = 'develop'
    final def FEATURE = 'feature'
    final def RELEASE = 'release'
    final def MASTER = 'master'
    final def HOTFIX = 'hotfix'
    final def UNKNOWN = null

    if (branchName == DEVELOP) {
        return DEVELOP
    } else if (branchName == MASTER) {
        return MASTER
    } else if (branchName.startsWith(FEATURE)) {
        return FEATURE
    } else if (branchName.startsWith(RELEASE)) {
        return RELEASE
    } else if (branchName.startsWith(HOTFIX)) {
        return HOTFIX
    } else {
        return UNKNOWN
    }
}

/**
 * Returns the tag of the current commit.
 */
String getCurrentTag() {
    try {
        sh "git describe --tags --exact-match > currentGitTag"
    } catch (Exception e) {
        //probably failed with "fatal: no tag exactly matches '7085589a8af6493fc9d0363257bf34d994549a93'"
        return null
    }
    return readFile('currentGitTag').trim()
}

String getProjectVersionFromBranchName(String branchName) {
    // correct branch names that contain a version number are: hotfix/1.2.3 and release/12.3.4
    def matches = branchName =~ '^(hotfix|release)/([^/]+)$'
    if (matches) {
        return matches[0][2]
    } else {
        throw new IllegalStateException("We expected that a branch with name ${branchName} contained a version number.")
    }
}

/**
 * Fails the build with an error message if certain environment variables are not available.
 * <p>
 * Note: {@code JBOSS_TST} and {@code JBOSS_ACC} are space-separated lists of
 * host names, e.g. {@code 'wildfly1 wildfly2'}
 * (see Manage Jenkins > Configure System > Global properties):
 */
void checkRequirements() {
    if (!env.ARTIFACTORY) {
        error 'Please set ARTIFACTORY to URL of target repo'
    }

    withCredentials([
            [$class          : 'UsernamePasswordMultiBinding', credentialsId: 'artifactory',
             usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
        if (!env.USERNAME || !env.PASSWORD) {
            error "Credentials for ID 'artifactory' not set"
        }
    }

    if (!env.JBOSS_TST) {
        error 'Please set JBOSS_TST to hostname(s) of Test deploy target(s)'
    }

    if (!env.JBOSS_ACC) {
        error 'Please set JBOSS_ACC to hostname(s) of User Acceptance Test deploy target(s)'
    }
}

/**
 * Runs Sonar analysis. Because the analysis of a project can't be run concurrently,
 * this task may fail. As this can happen fairly often, failure of this task is
 * permitted and a message is logged.
 */
void sonar() {
    stage name: 'sonar', concurrency: 1
    try {
        // Sanitize project / branch name
        // Allowed characters are alphanumeric, '-', '_', '.' and ':', with at least one non-digit.
        String branchName = env.BRANCH_NAME
                .replaceAll('/', '-') // e.g. feature-BC-123-Feature_Description
                .replaceAll('[^A-Za-z0-9_:\\.\\-]', '')

        maven "sonar:sonar  -DsonarBranch=${branchName}"
    } catch (hudson.AbortException e) {
        echo "Exception while running Sonar. Is there another SonarQube analysis in progress for this project?"
    }
}

/**
 * Generates and archives Javadoc.
 */
void javadoc() {
    stage name: 'javadoc'
    maven 'javadoc:javadoc'
    step $class: 'JavadocArchiver', javadocDir: 'target/site/apidocs', keepAll: false
}

void clean() {
    stage name: 'clean'
    maven 'clean'
}

void checkThatProjectVersionIs(String expectedProjectVersion) {
    stage name: 'correct project version?'
    def version = getVersion()
    if (!expectedProjectVersion.equals(version)) {
        error("Invalid project version, this project must be version: ${expectedProjectVersion} , but it is ${version}")
    }
}

void checkThatProjectIsRelease() {
    def version = getVersion()
    stage name: 'correct project version type: release?'
    if (version.contains('SNAPSHOT')) {
        error(
                "Invalid project version, this project cannot be a SNAPSHOT version in this branch. Project version: ${version} ")
    }
}

String getVersion() {
    sh script: 'sh mvnw org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version |egrep -v "(^\\[INFO\\])" | egrep -v "(^\\[WARNING\\])" |egrep -E "^[0-9]" > version'
    return readFile('version').trim()
}

String getGroup() {
    sh script: 'sh mvnw org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.groupId |egrep -v "(^\\[INFO\\])" | egrep -v "(^\\[WARNING\\])" > group'
    return readFile('group').trim()
}

void checkThatProjectIsSnapshot() {
    stage name: 'correct project version type: SNAPSHOT?'

    String version = getVersion()

    if (!version.endsWith('-SNAPSHOT')) {
        error('Project does not have a snapshot version')
    }
}

void checkThatProjectVersionMatchesTag() {
    String currentTag = getCurrentTag()
    if (!currentTag) {
        error(
                "Each commit on this branch should have a tag but this commit has none. Tag this commit correctly, and run this build again.")
    }
    println "Current commit has tag ${currentTag}, let's check if the project version matches that"
    checkThatProjectVersionIs(currentTag)
}

/**
 * Runs tests and archives the reports.
 */
void test() {
    stage name: 'test'
    try {
        maven "test -DKEYCLOAK_TEST_OAUTH_REQUEST_URI=${env.KEYCLOAK_TST} -DDOMAIN_ENVIRONMENT=tst -DAUTH_SERVER_URL=${env.AUTH_SERVER_URL} -Darquillian.launch=arquillian-cube"
        maven 'jacoco:merge'
    } catch (Exception e) {
        println "Test failed with exception " + e.class.name + " and message " + e.message
        throw e
    } finally {
        step $class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'
        def serverLogGeneratedByArquillianExists = fileExists 'build/log/server.log'
        if (serverLogGeneratedByArquillianExists) {
            step $class: 'ArtifactArchiver', artifacts: "build/log/server.log"
        }
    }
}

/**
 * Builds and captures the WAR file.
 */
void build() {
    stage name: 'build'
    // no need to test again, so -x test
    maven 'package -Dmaven.test.skip'
    step $class: 'ArtifactArchiver', artifacts: '**/target/*.war', fingerprint: true
}

def getWarName() {
    sh script: ' ls target |grep war |grep -v .original |grep -v swarm > warName'
    String warName = readFile('warName').trim()
    warName = warName.replaceAll("-\\d.*", ".war")
    return warName
}

void container() {
    stage name: 'build container', concurrency: 1
    maven 'fabric8:build'
}

void callStackStorm() {
    String domain = getWarName().replaceAll("\\.war", "")
    String version = getVersion()
    if (!warName.contains("-backend")) {
        warName = warName.tokenize('-').first()
    }
    def environment = 'tst'
    String domainType = getGroup().tokenize('.').last()
    def artifactoryRepository = 'wars-snapshot-local'
    if (!getVersion().contains("SNAPSHOT")) {
        environment = 'acc'
        artifactoryRepository = 'wars-release-local'
    }
    sh "java -jar /srv/jenkins/jenkins-cli.jar -s http://basecamp-jenkins-dev.crv4all.com:9090/ -i /srv/jenkins/id_jenkins_rsa build stackstorm/helixs_stackstorm_deploy -s -p ENVIRONMENT=${environment} -p ARTIFACTORY_REPO=${artifactoryRepository} -p DOMAIN=${domain} -p VERSION=${version} -p WAR_NAME=${domain} -p DOMAIN_TYPE=${domainType} -p BUILD_USER=basecamp"
}

void push() {
    stage name: 'push container', concurrency: 1
    maven 'fabric8:push -Ddocker.username=crv -Ddocker.password=Crv1234'
}

void buildFeatureBranch() {
    clean()
    test()
    sonar()
    javadoc()
    build()
}

void buildDevelopBranch(Map config) {
    clean()
    checkThatProjectIsSnapshot()
    test()
    build()
    sonar()
    javadoc()
    container()
    push()
}

void buildReleaseBranch(Map config) {
    clean()
    checkThatProjectVersionIs(getProjectVersionFromBranchName(env.BRANCH_NAME))
    test()
    build()
    sonar()
    javadoc()
    container()
    push()
}

void buildHotfixBranch(Map config) {
    clean()
    checkThatProjectVersionIs(getProjectVersionFromBranchName(env.BRANCH_NAME))
    test()
    build()
    sonar()
    javadoc()
    container()
    push()
}

void buildMasterBranch(Map config) {
    clean()
    checkThatProjectVersionMatchesTag()
    checkThatProjectIsRelease()
    test()
    build()
    sonar()
    javadoc()
    container()
    push()
}

void maven(String command) {
    sh "sh mvnw ${command}"
}

void execute(Map customConfig = [:]) {
    config = [deploySingleton: false, isAdapter: false]
    config << customConfig
//    def branchType = detectBranchType(env.BRANCH_NAME)

    echo "Building branch: ${env.BRANCH_NAME}\n - type: ${branchType}\n - build number: ${env.BUILD_NUMBER}"

//    checkRequirements()

//    switch (branchType) {
//        case 'feature':
            buildFeatureBranch()
//            break
//        case 'develop':
//            buildDevelopBranch(config)
//            break
//        case 'release':
//            buildReleaseBranch(config)
//            break
//        case 'master':
//            buildMasterBranch(config)
//            break
//        case 'hotfix':
//            buildHotfixBranch(config)
//            break
//        default:
//            error "Don't know what to do with this branch: ${env.BRANCH_NAME}"
    }
}

//keep only 10 builds, see https://issues.jenkins-ci.org/browse/JENKINS-34738
properties(
        [[$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactNumToKeepStr: '10', numToKeepStr: '10']]])

return this
