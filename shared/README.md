# Shared Build Scripts for Gradle and Jenkins Pipeline

*NB: version 5.0.0+ is for when you are using gradle 4.0+ _only_*

# Using Git Subtree

One of the simplest ways of using this repository inside your own project is to add it as a Git subtree in a `shared` folder.

1. Clone or create a repository

        mkdir my-project
        cd my-project
        git init

2. At least one commit is required before you can add a subtree. If you're starting a new project, you can use this

        touch README.md
        git add README.md
        git commit -m "Initial commit"

3. Add a remote, add the subtree to the `/shared` folder, and remove the remote

        # set the version you want to add
        export VERSION=9.5.0

        # add the remote
        git remote add build-scripts --no-tags git@bitbucket.org:crv4all/build-scripts.git
        # add the subtree into a directory named 'shared'
        git subtree add build-scripts $VERSION --prefix=shared --squash -m "Get build-scripts version $VERSION"
        # remove the remote again, so the tags of both remotes are not mixed when you do a fetch-with-all-tags
        git remote remove build-scripts

4. Copy the the `Jenkinsfile` and `build.gradle` from one of the readily available projects (take note, currently there are two types of projects: war- & library-projects):

        cp <path-to-your-other-project>/Jenkinsfile .

        cp <path-to-your-other-project>/build.gradle .

5. Edit the 'Jenkinsfile' and alter the script if needed. Use the `DefaultPipeline.groovy` for WAR projects and the `LibraryPipeline.groovy` for jar/library projects

6. You should now be able to do a build

        gradle build

## Updating to a newer version

When so desired, you can merge another version of the build-scripts:

    #set the version you want to add
    export VERSION=9.5.0

    #re-add the remote
    git remote add build-scripts --no-tags git@bitbucket.org:crv4all/build-scripts.git

    #merge the selected version
    git subtree pull build-scripts $VERSION --prefix=shared --squash -m "Get build-scripts version $VERSION"

    #remote the remote, so the tags of both remotes are not mixed when you do a fetch-with-all-tags
    git remote remove build-scripts

## Upgrading to 3.0.0

Breaking change: we're removing a lib name: you should think whether you want jackson version one or two. If you really don't want to think: pick 1, as that is what you had before.

In that case, replace:

libs.resteasy

by

libs.resteasyJacksonProvider

in your build.gradle

## Upgrading to 4.0.0

You can either:
- look at the merge done in housing-domain
- or follow the following steps:

1) Remove from your build.gradle:

    ext {
        defaultArquillianProfile = 'jboss-remote' // or 'jboss-managed'
        arquillianProfile = project.properties.arquillianProfile ?: defaultArquillianProfile
    }

and

    if (arquillianProfile == 'jboss-managed') {
        // If you want to start a new instance of Wildfly to run the tests, use this dependency
        // Note: If you don't specify a JBOSS_HOME environment variable, it looks for a folder in the parent directory of the
        // project called wildfly-${versions.wildfly}
        // NOTE: Also change parameter 'arquillian.launch'
        testRuntime group: 'org.wildfly.arquillian', name: 'wildfly-arquillian-container-managed', version: versions.wildflyArquillian

    } else {
        // If you want to test on a running (remote) instance of Wildfly, use this dependency instead.
        // Make sure to configure the host/port, username/password correctly in arquillian.xml
        // Also make sure that Wilfly is running on Java 8
        testRuntime group: 'org.wildfly.arquillian', name: 'wildfly-arquillian-container-remote', version: versions.wildflyArquillian
    }

and

    if (arquillianProfile == 'jboss-managed') {
        // Installation of Wildfly to use for testing; this can be set here or in the arquillian.xml
        println "Setting JBOSS_HOME to ${jbossHome}"
        environment 'JBOSS_HOME', jbossHome
        // systemProperty 'arq.container.wildfly.configuration.jbossHome', jbossHome
    }

    // The profile to launch (defined as container qualifier in arquillian.xml)
    // TODO: control this via command line maybe?
    println "Using Arquillian profile: ${arquillianProfile}"
    systemProperty 'arquillian.launch', arquillianProfile

2) Remove from your arquillian.xml:

    <container qualifier="jboss-remote-ci">

        <configuration>
            <!-- By default, arquillian will use the JBOSS_HOME environment variable.
                 Alternatively, the configuration below can be uncommented. -->
            <!--<property name="jbossHome">/path/to/wildfly</property> -->

            <property name="managementAddress">svbcp001t.adinfra.crv4all.com</property>
            <property name="managementPort">9990</property>
            <property name="username">basecamp</property>
            <property name="password">basecamp</property>

        </configuration>
    </container>

    <container qualifier="jboss-managed">
        <configuration>
            <!-- This assumes you're running against a Keycloak host+client -->
            <property name="serverConfig">standalone-keycloak.xml</property>
        </configuration>
    </container>

3) Add to your arquillian.xml:



    <extension qualifier="docker">
        <property name="serverVersion">1.12</property>
        <property name="definitionFormat">CUBE</property>
        <property name="serverUri">http://${dockerServer}:2375</property>
        <property name="clean">true</property>
    </extension>
    <container qualifier="${containerName}" >
        <configuration>
            <property name="managementAddress">${dockerServer}</property>
            <property name="username">basecamp</property>
            <property name="password">basecamp</property>
        </configuration>
    </container>

4) Add a file src/test/resources/logback-test.xml with contents:

    <configuration>
        <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
            <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
                <pattern>%msg%n</pattern>
            </encoder>
        </appender>
        <logger name="com.crv" level="DEBUG">
            <appender-ref ref="CONSOLE" />
        </logger>
        <root level="INFO">
            <appender-ref ref="CONSOLE"/>
        </root>
    </configuration>

5) Make sure your "version=X.Y.Z-SNAPSHOT" line is above the "apply from: gradleScripts + 'common-build-war.gradle'" line

6) Replace libs.resteasyClient from you testCompile line by libs.jerseyClient

## Upgrading to 5.0.0
For upgrading to 5.0.0, a new gradle wrapper is needed. This is done by executing:

    ./gradlew wrapper --gradle-version=4.0.1
